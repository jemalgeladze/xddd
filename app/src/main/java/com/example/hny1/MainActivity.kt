package com.example.hny1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

private lateinit var editTextNote: EditText
private lateinit var addNoteButton: Button
private lateinit var textView: TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPreferences = getSharedPreferences("MY_NOTES_PREF", MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTE", "1")
        textView.text = text
        editTextNote = findViewById(R.id.editTextNote)
        addNoteButton = findViewById(R.id.addNoteButton)
        textView = findViewById(R.id.textView)
        addNoteButton.setOnClickListener {
            val note = editTextNote.text.toString()
            val text = textView.text.toString()
            val result = note + "/n" + text
            textView.text = result
            sharedPreferences.edit()
                .putString("NOTE", result)
                .apply()
        }
    }
}