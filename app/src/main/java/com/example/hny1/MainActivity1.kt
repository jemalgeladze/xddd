package com.example.hny

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TableLayout
import androidx.viewpager.widget.ViewPager
import com.example.hny1.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager
    private lateinit var tableLayout: TableLayout
    private lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        viewPager = findViewById(R.id.viewPager)
        tableLayout = findViewById(R.id.tableLayout)


        TabLayoutMediator(tableLayout, viewPager){tab,position ->
            tab.text = "tab ${position + 1}"

        }.attach()
    }



    private fun TabLayoutMediator(
        tableLayout: TableLayout?,
        viewPager: ViewPager?,
        function: (tab: TabLayout.Tab, position: Int) -> Unit
    ): TabLayoutMediator {
        TODO("Not yet implemented")
    }
}