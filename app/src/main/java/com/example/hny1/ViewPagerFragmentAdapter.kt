package com.example.hny

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter


abstract class ViewPagerFragmentAdapter(activity:FragmentActivity) : FragmentStateAdapter(activity){
    override fun getItemCount(): Int {

        return 2
    }

    override fun createFragment(position: Int): Fragment {
        when(position){
            0 -> Fragment()
            1 -> Fragment()
            else -> Fragment()
        }
        TODO("Not yet implemented")
    }
}